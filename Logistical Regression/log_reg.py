import pandas as pd
import seaborn as sns
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression

# Import the data
ad_data = pd.read_csv('advertising.csv')

# Pairplot to show correlations
pp1 = sns.pairplot(ad_data, hue='Clicked on Ad')
pp1.savefig('correlations.png')

# Clean the data and categorize as much as possible to make model more accurate
countries = pd.get_dummies(ad_data['Country'], drop_first=True)
topic = pd.get_dummies(ad_data['Ad Topic Line'], drop_first=True)

ad_data = pd.concat([ad_data, countries, topic], axis=1)

# Drop Unnecessary data
ad_data.drop(['Ad Topic Line', 'City', 'Country', 'Timestamp'], axis=1, inplace=True)

# Separate the data into predictors and results
X = ad_data.drop('Clicked on Ad', axis=1)
y = ad_data['Clicked on Ad']

# Split into train and test data
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=101)

# fit the model
logmodel = LogisticRegression()
logmodel.fit(X_train, y_train)

# test the model
predictions = logmodel.predict(X_test)

report = classification_report(y_test, predictions)
f = open('class_report.txt', 'w')
f.write(report)
f.close()
