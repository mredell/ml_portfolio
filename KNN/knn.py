# -*- coding: utf-8 -*-
"""
Created on Fri May 17 13:17:57 2019

@author: Matt Redell
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report, confusion_matrix

# Import the data
df = pd.read_csv('KNN_Project_Data')

# Scale the data
scaler = StandardScaler()
scaler.fit(df.drop('TARGET CLASS', axis=1))
scaled_feat = scaler.transform(df.drop('TARGET CLASS', axis=1))
df_feat = pd.DataFrame(scaled_feat, columns=df.columns[:-1])

# Split data into training an testing
X = df_feat
y = df['TARGET CLASS']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=101)

# Produce a model for K=1
knn = KNeighborsClassifier(n_neighbors=1)
knn.fit(X_train, y_train)
pred = knn.predict(X_test)

conf = confusion_matrix(y_test, pred)
np.savetxt('K1report.txt', conf)
f1 = open('K1report.txt', 'a')
f1.write('WITH K=1')
f1.write('\n')
f1.write(classification_report(y_test, pred))
f1.close()

# Test multiple K values to choose best one
error_rate = []

for i in range(1, 40):
    knn = KNeighborsClassifier(n_neighbors=i)
    knn.fit(X_train, y_train)
    pred = knn.predict(X_test)
    error_rate.append(np.mean(pred != y_test))

# Plot out the results
plt.figure(figsize=(30, 15))
plt.plot(range(1,40), error_rate, color='b', linestyle='--', marker='o', markerfacecolor='r', markersize=10)
plt.title('Error Rate v K Value')
plt.xlabel('K')
plt.ylabel('Error Rate')
plt.savefig('Ksample.png')

# Retrain with better K-value selection
knn = KNeighborsClassifier(n_neighbors=39)
knn.fit(X_train, y_train)
pred = knn.predict(X_test)

conf = confusion_matrix(y_test, pred)
np.savetxt('refinedK.txt', conf)
f2 = open('refinedK.txt', 'a')
f2.write('WITH K=39')
f2.write('\n')
f2.write(classification_report(y_test, pred))
f2.close()
