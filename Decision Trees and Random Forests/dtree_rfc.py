# -*- coding: utf-8 -*-
"""
Created on Wed May 22 16:50:51 2019

@author: Matt
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.ensemble import RandomForestClassifier
from sklearn.externals.six import StringIO
from IPython.display import Image
from sklearn.tree import export_graphviz
import pydot

# Load the data
loans = pd.read_csv('loan_data.csv')

# Process data and get dummy variables for strings
cat_feats = ['purpose']
final_data = pd.get_dummies(loans,columns=cat_feats,drop_first=True)

# Set up training data and testing data
X = final_data.drop('credit.policy',axis=1)
y = final_data['credit.policy']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=101)

# Train decision tree
dtree = DecisionTreeClassifier()
dtree.fit(X_train,y_train)

# Predict and evaluate
pred = dtree.predict(X_test)

# print reports to file
conf = confusion_matrix(y_test, pred)
np.savetxt('dtreereport.txt', conf)
f1 = open('dtreereport.txt', 'a')
f1.write('\n')
f1.write(classification_report(y_test, pred))
f1.close()

# Make a flowchart of decision tree
features = list(final_data.columns[1:])
with open("loans_tree.dot", "w") as f:
    f = export_graphviz(dtree, out_file=f,feature_names=features,filled=True,rounded=True)
# run 'dot -Tpdf .\loans_tree.dot -o loans_tree.pdf' in terminal

# Now, let's do the random forests model
rfc = RandomForestClassifier(n_estimators=600)
rfc.fit(X_train,y_train)

# Make predictions on test data
rfc_pred = rfc.predict(X_test)

# Write performance reports to file
rfc_conf = confusion_matrix(y_test, rfc_pred)
np.savetxt('rfcreport.txt', rfc_conf)
f2 = open('rfcreport.txt', 'a')
f2.write('\n')
f2.write(classification_report(y_test, rfc_pred))
f2.close()