# Matthew Redell Machine Learning Portfolio
Here are a few different projects and examples using machine learning that I have used.
They are derived from exercises in the Udemy class "Python for Data Science and Machine Learning Bootcamp"
by Jose Portilla, Pierian Data international

## Linear Regression
*Linear regression* is the method of fitting data to a line, with some
level of irreducible error. It can be used to make numerical predictions based on
linear trends. 

In this set, I worked through e-commerce customer data to analyze trends. 
Specifically, to analyze whether the company would be wiser to spend time on developing
their website or mobile app.
In addition to the source code used, you will find:
* The 'Ecommerce Customers' csv file used for analysis
* Plots for correlation between spending on the App and Website (independently)
* The correlations between all numerical elements in the data
* The coefficients of the linear model in _coefficients.txt_
* The errors of the model in _errors.txt_
* The residuals of the model in a distribution plot

## Logistical Regression
*Logistical regression* is used to classify data. You can use these
models to predict a binary option (i.e. true or false)

In this set, I worked through data detailing certain users and whether
or not they clicked on an ad, based on location, type of ad, age, etc.
In addition to the source code, you will find:
* The data in *advertising.csv*
* The pairplot for correlations between all numerical variables
* The classification report for the confusion matrix

## K-Nearest Neighbors
*K-Nearest Neighbors* (KNN) is another method for classifying data
based on neighboring data points. It can be used to predict what 
category a certain data point may fall into based on its neighbors.

In this project, I took a dataset of classified (unknown) data
and classified it based on the target values 0 or 1. In addition to the
source code, you will find:
* The dataset in *KNN_Project_Data.csv*
* The classification report and confusion matrix for K=1
* The plot for sampling K values
* The classification report anf confucion matrix for the refined K value (39)

## Decision Trees and Random Forests
*Decision trees and Random Forests* are used to classify data that depends on a number of variables and is quite easy to deploy.
Simply put, it asks a question, then moves down the tree depending on whether the answer to the question is
'yes' or 'no'.

In this project, I worked through the provided data set detailing loan applications on LendingClub.com. Using 
the provided data, I trained models to predict whether or not an applicant would obtain a loan based on how likely
they were to pay back the loan in full. In addition to the source code, you will find:
* The dataset in *loan_data.csv*
* The classifaction report and confucion matrix for both the single decision tree as well as the random forests
* The Tree Diagram for the trained Decision Tree model