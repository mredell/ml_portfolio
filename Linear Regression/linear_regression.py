import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn import metrics

# Read the data from the csv
customers = pd.read_csv('Ecommerce Customers')

# Do some data analysis in plots

jp1 = sns.jointplot(data=customers, x='Time on Website', y='Yearly Amount Spent')
jp2 = sns.jointplot(data=customers, x='Time on App', y='Yearly Amount Spent')
jp1.savefig('webspend.png')
jp2.savefig('appspend.png')

# Analyze all of the data quickly
pp = sns.pairplot(customers)
pp.savefig('correlation.png')

# Split the data into training and testing sets
X = customers[['Avg. Session Length', 'Time on App','Time on Website', 'Length of Membership']]
y = customers[['Yearly Amount Spent']]

X_test, X_train, y_test, y_train = train_test_split(X, y, test_size=0.3, random_state=101)

# Fit the model for the training data
lm = LinearRegression()
lm.fit(X_train, y_train)

# Test the model
predictions = lm.predict(X_test)
fig, ax = plt.subplots()
ax.scatter(y_test, predictions)
ax.set_xlabel('Actual')
ax.set_ylabel('Predicted')
fig.savefig('testing.png')
plt.close()

# Evaluate the errors of the model and plot residuals
mae = metrics.mean_absolute_error(y_test, predictions)
mse = metrics.mean_squared_error(y_test, predictions)
rmse = np.sqrt(mse)
f = open('errors.txt', 'w')
f.write('MAE: %s\nMSE: %s\nRMSE: %s' % (mae, mse, rmse))
f.close()

fig, ax = plt.subplots()
sns.distplot(y_test - predictions, ax=ax)
fig.savefig('residuals.png')


# Show the coefficients to analyze trends
trends = pd.DataFrame(lm.coef_[0],X.columns,columns=['Coefficients'])
trends.to_csv('coefficients.txt', header=True, sep='\t')

